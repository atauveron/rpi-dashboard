use std::{collections::HashMap, time::Duration};

use serde::Deserialize;

use serde_json::json;

lazy_static! {
    static ref NAME_TO_SHORT: HashMap<&'static str, &'static str> =
        [("GARE DE PARIS MONTPARNASSE", "MONTPARNASSE"),]
            .iter()
            .cloned()
            .collect();
}

#[allow(dead_code, non_snake_case)]
#[derive(Deserialize)]
// Some types are catch-all types (usually String). More precise types (eg enums) would be better.
struct Disruption {
    id: Option<String>,
    creationDate: Option<String>,
    updateDate: Option<String>,
    title: String,
    r#type: String,
}

#[allow(dead_code, non_snake_case)]
#[derive(Deserialize)]
// Some types are catch-all types (usually String). More precise types (eg enums) would be better.
struct TrainInfo {
    modeTransportEnum: String,
    lineTransport: String,
    typeTrain: String,
    codeMission: String,
    canceled: bool,
    departureDate: String,
    departureTime: String,
    arrivalTime: String,
    destinationMission: String,
    platform: String,
    hasTraficDisruption: bool,
    hasTravauxDisruption: bool,
    hasHorsPerturbationDisruption: bool,
    disruptions: Vec<Disruption>,
}

#[allow(dead_code, non_snake_case)]
#[derive(Deserialize)]
struct StopArea {
    codeUic7: String,
    label: String,
}

#[allow(dead_code, non_snake_case)]
#[derive(Deserialize)]
struct NextTrains {
    platformAvailable: bool,
    disruptionsAvailable: bool,
    arrivalTimeAvailable: bool,
    nextTrainsList: Vec<TrainInfo>,
    departureStopArea: Option<StopArea>,
    arrivalStopArea: Option<StopArea>,
}

/// An upcoming train
pub struct Train {
    pub time: String,
    pub mission: String,
    pub destination: String,
    pub has_disruption: bool,
}

/// Call the Transilien API
///
/// # Arguments
///
/// - `name` - the departure station's name
/// - `uic` - the departure station's UIC7 code
///
/// # Return
///
/// A `NextTrains` structure, or a `String` in case of error
///
fn call(name: &str, uic: &str) -> Result<NextTrains, String> {
    let target = "https://www.transilien.com/api/nextDeparture/search";
    let body = json!({
        "departure": name,
        "uicArrival": String::new(),
        "uicDeparture": String::from(uic),
    });
    let referer = format!("https://www.transilien.com/fr/horaires/prochains-departs/?departure={}&uicArrival=&uicDeparture={}", name, uic);
    let response = ureq::post(target)
        .set("Accept", "application/json, text/plain, */*")
        .set("Accept-Encoding", "gzip, deflate, br")
        .set("Accept-Language", "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3")
        .set("DNT", "1")
        .set("Host", "www.transilien.com")
        .set("Origin", "https://www.transilien.com")
        .set("Referer", &referer)
        .set("TE", "trailers")
        .timeout(Duration::from_secs(10))
        .send_json(body);
    let response = match response {
        Ok(resp) => resp,
        Err(e) => return Err(format!("Request failed: {}", e)),
    };
    if response.status() < 200 || response.status() > 299 {
        return Err(format!(
            "Request failed: {} {}",
            response.status(),
            response.status_text()
        ));
    }
    let next_trains = response.into_json::<NextTrains>();
    match next_trains {
        Ok(res) => Ok(res),
        Err(err) => Err(err.to_string()),
    }
}

/// Convert the full station name to a short version
///
/// # Arguments
///
/// - `name` - the full station name (as returned by the Transilien API)
///
/// # Return
///
/// A short station name, or the original name if no short version is available
///
fn short_name(name: String) -> String {
    if NAME_TO_SHORT.contains_key(name.as_str()) {
        String::from(*NAME_TO_SHORT.get(name.as_str()).unwrap_or(&"?"))
    } else {
        name
    }
}

/// Fetch the upcoming trains for the given station from the Transilien API
///
/// # Arguments
///
/// - `name` - the departure station's name
/// - `uic` - the station's UIC7 code
///
/// # Return
///
/// A vector of `Train` structures, or a `String` in case of error
///
pub fn get_trains(name: &str, uic: &str) -> Result<Vec<Train>, String> {
    let trains = call(name, uic)?.nextTrainsList;
    let mut upcoming = Vec::<Train>::with_capacity(trains.len());
    for train in trains {
        if train.canceled {
            // If a train has been canceled, do not add it
            continue;
        };
        upcoming.push(Train {
            time: train.departureTime,
            mission: train.codeMission,
            destination: short_name(train.destinationMission),
            has_disruption: train.hasTraficDisruption
                || train.hasTravauxDisruption
                || train.hasHorsPerturbationDisruption,
        });
    }
    Ok(upcoming)
}

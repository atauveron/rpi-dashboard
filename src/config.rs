use std::fs::read_to_string;

use serde::Deserialize;

const CONFIG_FILE: &str = "config.json";

/// Application configuration
#[derive(Deserialize)]
pub struct Config {
    /// The latitude of the location for OpenWeather
    pub lat: f32,
    /// The longitude of the location for OpenWeather
    pub lon: f32,
    /// The API key for OpenWeather
    pub openweather_key: String,
    /// The name of the Transilien station
    pub station_name: String,
    /// The UIC7 code of the Transilien station
    pub station_id: String,
}

/// Read the application configuration from the configuration file, `config.json`
pub fn read_config() -> Config {
    let data = read_to_string(CONFIG_FILE).unwrap();
    serde_json::from_str(&data).unwrap()
}

use epd_waveshare::{epd4in2::Epd4in2, prelude::*};
use linux_embedded_hal::{sysfs_gpio::Direction, Delay, Pin, Spidev};

// Use Broadcom pin numbering
const CS_PIN: u64 = 5;
const BUSY_PIN: u64 = 24;
const DC_PIN: u64 = 25;
const RST_PIN: u64 = 17;

/// Initialise the display
///
/// # Arguments
///
/// - `spi` - an `Spidev` struct
/// - `delay` - a `Delay` struct
///
/// # Return
///
/// An `Epd4in2` struct representing the initialised display
///
pub fn init(spi: &mut Spidev, delay: &mut Delay) -> Epd4in2<Spidev, Pin, Pin, Pin, Pin, Delay> {
    // Configure Pin to be used as Chip Select for SPI
    let cs = Pin::new(CS_PIN);
    cs.export().expect("Failure - CS export");
    while !cs.is_exported() {}
    cs.set_direction(Direction::Out)
        .expect("Failure - CS set direction");
    cs.set_value(1).expect("Failure - CS set value (to 1)");

    // Configure Pin to be used as Busy for SPI
    let busy = Pin::new(BUSY_PIN);
    busy.export().expect("Failure - BUSY export");
    while !busy.is_exported() {}
    busy.set_direction(Direction::In)
        .expect("Failure - Busy set direction");

    // Configure Pin to be used as Data/Command for SPI
    let dc = Pin::new(DC_PIN);
    dc.export().expect("Failure - DC export");
    while !dc.is_exported() {}
    dc.set_direction(Direction::Out)
        .expect("Failure - DC set direction");
    dc.set_value(1).expect("Failure - DC set value (to 1)");

    // Configure Pin to be used as Reset for SPI
    let rst = Pin::new(RST_PIN);
    rst.export().expect("Failure - RST export");
    while !rst.is_exported() {}
    rst.set_direction(Direction::Out)
        .expect("Failure - RST set direction");
    rst.set_value(1).expect("Failure - RST set value (to 1)");

    // Initialise the display
    Epd4in2::new(spi, cs, busy, dc, rst, delay).expect("Failure - E-Ink initialisation")
}

#[cfg(test)]
mod tests {
    use super::*;

    use embedded_graphics::{
        mono_font::{ascii::FONT_6X10, MonoTextStyleBuilder},
        prelude::*,
        text::Text,
    };
    use embedded_hal::prelude::*;
    use epd_waveshare::{color::*, epd4in2::Display4in2};
    use linux_embedded_hal::spidev::{SpiModeFlags, SpidevOptions};

    #[test]
    fn display_basic() {
        // Configure SPI
        let mut spi = Spidev::open("/dev/spidev0.0").expect("Failure - SPI directory");
        let options = SpidevOptions::new()
            .bits_per_word(8)
            .max_speed_hz(4_000_000)
            .mode(SpiModeFlags::SPI_MODE_0)
            .build();
        spi.configure(&options)
            .expect("Failure - SPI configuration");

        // Initialise the display
        let mut delay = Delay {};
        let mut epd = init(&mut spi, &mut delay);

        // Display some text
        let mut display = Display4in2::default();
        let style = MonoTextStyleBuilder::new()
            .font(&FONT_6X10)
            .text_color(Black)
            .background_color(White)
            .build();
        let _ = Text::new("Hello from Rust", Point::new(5, 50), style).draw(&mut display);

        epd.update_frame(&mut spi, display.buffer(), &mut delay)
            .expect("Failure - E-Ink update frame");
        epd.display_frame(&mut spi, &mut delay)
            .expect("Failure - E-Ink display frame");
        epd.sleep(&mut spi, &mut delay)
            .expect("Failure - E-Ink sleep");

        // Wait before clearing
        delay.delay_ms(2_000u16);
        epd.wake_up(&mut spi, &mut delay)
            .expect("Failure - E-Ink wake up");
        epd.clear_frame(&mut spi, &mut delay)
            .expect("Failure - E-Ink clear");

        // Shut down the display
        epd.sleep(&mut spi, &mut delay)
            .expect("Failure - E-Ink sleep");
    }
}

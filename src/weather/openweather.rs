use std::collections::HashMap;
use std::time::Duration;

use serde::Deserialize;

use super::common::*;
use super::*;

lazy_static! {
    static ref CODE_TO_IMAGE: HashMap<u8, &'static [u8; 288]> = [
        (1, include_bytes!("../../assets/sun.bin")), // clear sky
        (2, include_bytes!("../../assets/cloudy-day.bin")), // few clouds
        (3, include_bytes!("../../assets/cloudy.bin")), // scattered clouds
        (4, include_bytes!("../../assets/cloud.bin")), // broken clouds
        (9, include_bytes!("../../assets/rain.bin")), // shower rain
        (10, include_bytes!("../../assets/rainy.bin")), // rain
        (11, include_bytes!("../../assets/thunder.bin")), // thunderstorm
        (13, include_bytes!("../../assets/snow.bin")), // snow
        (50, include_bytes!("../../assets/mist.bin")), // mist
    ]
    .iter()
    .cloned()
    .collect();
}

#[allow(dead_code)]
#[derive(Deserialize)]
struct WeatherType {
    id: u16,
    main: String,
    description: String,
    icon: String,
}

#[allow(dead_code)]
#[derive(Deserialize)]
struct Temp {
    morn: f32,
    day: f32,
    eve: f32,
    night: f32,
    min: f32,
    max: f32,
}

#[allow(dead_code)]
#[derive(Deserialize)]
struct Current {
    dt: i64,
    temp: f32,
    pressure: u16,
    humidity: u16,
    clouds: u8,
    wind_speed: f32,
    wind_gust: Option<f32>,
    wind_deg: u16,
    weather: Vec<WeatherType>,
}

#[allow(dead_code)]
#[derive(Deserialize)]
struct ForecastHourly {
    dt: i64,
    temp: f32,
    pressure: u16,
    humidity: u16,
    clouds: u8,
    wind_speed: f32,
    wind_gust: Option<f32>,
    wind_deg: u16,
    weather: Vec<WeatherType>,
    pop: f32,
}

#[allow(dead_code)]
#[derive(Deserialize)]
struct ForecastDaily {
    dt: i64,
    temp: Temp,
    pressure: u16,
    humidity: u16,
    clouds: u8,
    wind_speed: f32,
    wind_gust: Option<f32>,
    wind_deg: u16,
    weather: Vec<WeatherType>,
    pop: f32,
}

#[allow(dead_code)]
#[derive(Deserialize)]
struct OneCallResponse {
    lat: f32,
    lon: f32,
    timezone: String,
    timezone_offset: i32,
    current: Current,
    hourly: Vec<ForecastHourly>,
    daily: Vec<ForecastDaily>,
}

/// Convert the icon code returned by the OpenWeather API to a corresponding image
fn code_to_image(icon: &str) -> &'static [u8; 288] {
    let code = icon
        .trim_end_matches('d')
        .trim_end_matches('n')
        .parse::<u8>()
        .unwrap_or(0);
    CODE_TO_IMAGE.get(&code).unwrap_or(&&[0u8; 288])
}

/// Query the OpenWeather OneCall API
///
/// # Arguments
///
/// - `lat` - the latitude (in degrees)
/// - `lon` - the longitude (in degrees)
/// - `key` - the API key
///
/// # Return
///
/// A `OneCallResponse` structure, or a `String` in case of error
///
fn query_onecall(lat: f32, lon: f32, key: &str) -> Result<OneCallResponse, String> {
    let target = format!(
        "https://api.openweathermap.org/data/2.5/onecall?lat={}&lon={}&appid={}&units=metric&exclude=minutely,alerts",
        lat, lon, key
    );
    let response = ureq::get(&target).timeout(Duration::from_secs(10)).call();
    let response = match response {
        Ok(resp) => resp,
        Err(e) => return Err(format!("Request failed: {}", e)),
    };
    if response.status() < 200 || response.status() > 299 {
        return Err(format!(
            "Request failed: {} {}",
            response.status(),
            response.status_text()
        ));
    }
    let obs = response.into_json::<OneCallResponse>();
    match obs {
        Ok(res) => Ok(res),
        Err(err) => Err(err.to_string()),
    }
}

/// Fetch the weather (both current weather and forecast) for the given location from OpenWeather
///
/// # Arguments
///
/// - `lat` - the latitude (in degrees)
/// - `lon` - the longitude (in degrees)
/// - `key` - the API key
///
/// # Return
///
/// A `Weather` structures, or a `String` in case of error
///
pub fn get_openweather(lat: f32, lon: f32, key: &str) -> Result<Weather, String> {
    let response = query_onecall(lat, lon, key)?;
    let weather: &WeatherType = match response.current.weather.len() {
        0 => return Err(String::from("Current weather - Empty weather vector")),
        1 => &response.current.weather[0],
        _ => {
            log::info!("Current weather - More than one element in weather vector. Using first");
            &response.current.weather[0]
        }
    };
    let wind = match response.current.wind_gust {
        Some(wind_gust) => format!(
            "{} {:.0}-{:.0}m/s",
            wind_direction_to_text(response.current.wind_deg),
            response.current.wind_speed,
            wind_gust
        ),
        None => format!(
            "{} {:.0}m/s",
            wind_direction_to_text(response.current.wind_deg),
            response.current.wind_speed
        ),
    };
    let report = WeatherReport {
        icon: code_to_image(&weather.icon),
        temp: format!("{:.1}°", response.current.temp),
        wind,
    };
    let mut hourly = Vec::<WeatherForecast>::new();
    for item in response.hourly {
        let date = match timestamp_to_datetime(item.dt) {
            Some(dt) => format!("{}", dt.format("%H:%M")),
            None => String::from("?"),
        };
        let weather: &WeatherType = match item.weather.len() {
            0 => return Err(String::from("Hourly forecast - Empty weather vector")),
            1 => &item.weather[0],
            _ => {
                log::info!(
                    "Hourly forecast - More than one element in weather vector. Using first"
                );
                &item.weather[0]
            }
        };
        let wind = match item.wind_gust {
            Some(wind_gust) => format!(
                "{} {:.0}-{:.0}m/s",
                wind_direction_to_text(item.wind_deg),
                item.wind_speed,
                wind_gust
            ),
            None => format!(
                "{} {:.0}m/s",
                wind_direction_to_text(item.wind_deg),
                item.wind_speed
            ),
        };
        hourly.push(WeatherForecast {
            date,
            icon: code_to_image(&weather.icon),
            temp: format!("{:.1}°", item.temp),
            wind,
        })
    }
    let mut daily = Vec::<WeatherForecast>::new();
    for item in response.daily {
        let date = match timestamp_to_datetime(item.dt) {
            Some(dt) => format!("{}", dt.format("%A")),
            None => String::from("?"),
        };
        let weather: &WeatherType = match item.weather.len() {
            0 => return Err(String::from("Daily forecast - Empty weather vector")),
            1 => &item.weather[0],
            _ => {
                log::info!("Daily forecast - More than one element in weather vector. Using first");
                &item.weather[0]
            }
        };
        let wind = match item.wind_gust {
            Some(wind_gust) => format!(
                "{} {:.0}-{:.0}m/s",
                wind_direction_to_text(item.wind_deg),
                item.wind_speed,
                wind_gust
            ),
            None => format!(
                "{} {:.0}m/s",
                wind_direction_to_text(item.wind_deg),
                item.wind_speed
            ),
        };
        daily.push(WeatherForecast {
            date,
            icon: code_to_image(&weather.icon),
            temp: format!("{:.0}°/{:.0}°", item.temp.min, item.temp.max),
            wind,
        })
    }
    Ok(Weather {
        report,
        hourly,
        daily,
    })
}

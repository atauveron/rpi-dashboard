use std::collections::HashMap;
use std::time::Duration;

use meteofrance_rs::model::forecast_v2;
use meteofrance_rs::url::{forecast_v2_url, Language};

use super::common::*;
use super::*;

#[derive(Clone, Default, Eq, Hash, PartialEq)]
enum WeatherIcon {
    #[default]
    None,
    ClearSky,
    // FewClouds,
    ScatteredClouds,
    BrokenClouds,
    Mist,
    Rain,
    ShowerRain,
    Thunderstorm,
    Snow,
    Hail,
}

lazy_static! {
    static ref ICON_TO_IMAGE: HashMap<WeatherIcon, &'static [u8; 288]> = [
        (
            WeatherIcon::ClearSky,
            include_bytes!("../../assets/sun.bin")
        ),
        // (
            // WeatherIcon::FewClouds,
            // include_bytes!("../../assets/cloudy-day.bin")
        // ),
        (
            WeatherIcon::ScatteredClouds,
            include_bytes!("../../assets/cloudy.bin")
        ),
        (
            WeatherIcon::BrokenClouds,
            include_bytes!("../../assets/cloud.bin")
        ),
        (WeatherIcon::Mist, include_bytes!("../../assets/mist.bin")),
        (WeatherIcon::Rain, include_bytes!("../../assets/rainy.bin")),
        (
            WeatherIcon::ShowerRain,
            include_bytes!("../../assets/rain.bin")
        ),
        (
            WeatherIcon::Thunderstorm,
            include_bytes!("../../assets/thunder.bin")
        ),
        (WeatherIcon::Snow, include_bytes!("../../assets/snow.bin")),
        (WeatherIcon::Hail, include_bytes!("../../assets/snow.bin")), // TODO Find an asset for hail!
    ]
    .iter()
    .cloned()
    .collect();
}

lazy_static! {
    static ref CODE_TO_ICON: HashMap<u8, WeatherIcon> = [
        (u8::default(), WeatherIcon::default()),
        (1, WeatherIcon::ClearSky),
        (2, WeatherIcon::ScatteredClouds),
        (3, WeatherIcon::BrokenClouds),
        (4, WeatherIcon::BrokenClouds),
        (5, WeatherIcon::Mist),
        (6, WeatherIcon::Mist),
        (7, WeatherIcon::Mist),
        (8, WeatherIcon::Mist),
        (9, WeatherIcon::Rain),
        (10, WeatherIcon::Rain),
        (11, WeatherIcon::Rain),
        (12, WeatherIcon::Rain),
        (13, WeatherIcon::Rain),
        (14, WeatherIcon::ShowerRain),
        (15, WeatherIcon::ShowerRain),
        (16, WeatherIcon::Thunderstorm),
        (17, WeatherIcon::Snow),
        (18, WeatherIcon::Snow),
        (19, WeatherIcon::Snow),
        (20, WeatherIcon::Snow),
        (21, WeatherIcon::Snow),
        (22, WeatherIcon::Snow),
        (23, WeatherIcon::Snow),
        (24, WeatherIcon::Hail),
        (25, WeatherIcon::Hail),
        (26, WeatherIcon::Thunderstorm),
        (27, WeatherIcon::Thunderstorm),
        (28, WeatherIcon::Thunderstorm),
        (29, WeatherIcon::Thunderstorm),
    ]
    .iter()
    .cloned()
    .collect();
}

/// Convert the icon code returned by the MeteoFrance API to a corresponding image
fn icon_to_image(icon: &str) -> &'static [u8; 288] {
    let digits: String = icon
        .trim_start_matches('p')
        .chars()
        .filter(|char| char.is_ascii_digit())
        .collect();
    let code = digits.parse::<u8>().unwrap_or_default();
    let weather = match CODE_TO_ICON.get(&code) {
        Some(value) => value.clone(),
        None => WeatherIcon::default(),
    };
    ICON_TO_IMAGE.get(&weather).unwrap_or(&&[0u8; 288])
}

/// Query the MeteoFrance forecast v2 API
///
/// # Arguments
///
/// - `lat` - the latitude (in degrees)
/// - `lon` - the longitude (in degrees)
/// - `key` - the API key
///
/// # Return
///
/// A `ForecastResponse` structure, or a `String` in case of error
///
fn query_forecast_v2(
    lat: f32,
    lon: f32,
    key: Option<&str>,
) -> Result<forecast_v2::ForecastResponse, String> {
    let target = forecast_v2_url(lat, lon, Some(Language::French), key);
    let response = ureq::get(&target).timeout(Duration::from_secs(10)).call();
    let response = match response {
        Ok(resp) => resp,
        Err(e) => return Err(format!("Request failed: {}", e)),
    };
    if response.status() < 200 || response.status() > 299 {
        return Err(format!(
            "Request failed: {} {}",
            response.status(),
            response.status_text()
        ));
    }
    let obs = response.into_json::<forecast_v2::ForecastResponse>();
    match obs {
        Ok(res) => Ok(res),
        Err(err) => Err(err.to_string()),
    }
}

/// Fetch the weather (both current weather and forecast) for the given location from MeteoFrance
///
/// # Arguments
///
/// - `lat` - the latitude (in degrees)
/// - `lon` - the longitude (in degrees)
/// - `key` - the API key
///
/// # Return
///
/// A `Weather` structures, or a `String` in case of error
///
pub fn get_meteofrance(lat: f32, lon: f32, key: Option<&str>) -> Result<Weather, String> {
    let response = query_forecast_v2(lat, lon, key)?;
    // TODO Proper error handling
    // How to find current weather using MétéoFrance???
    let report = WeatherReport {
        icon: &[0u8; 288],
        temp: String::new(),
        wind: String::new(),
    };
    let mut hourly = Vec::<WeatherForecast>::new();
    // Hourly forecast
    for item in response.properties.forecast {
        let date = item.time; // TODO Formatting
        let wind = match item.wind_speed_gust {
            Some(wind_speed_gust) => format!(
                "{} {:.0}-{:.0}m/s",
                wind_direction_to_text(
                    item.wind_direction
                        .unwrap_or_default()
                        .try_into()
                        .unwrap_or_default()
                ),
                item.wind_speed.unwrap_or_default(),
                wind_speed_gust
            ),

            None => format!(
                "{} {:.0}m/s",
                wind_direction_to_text(
                    item.wind_direction
                        .unwrap_or_default()
                        .try_into()
                        .unwrap_or_default()
                ),
                item.wind_speed.unwrap_or_default()
            ),
        };
        hourly.push(WeatherForecast {
            date,
            icon: icon_to_image(&item.weather_icon.unwrap_or_default()),
            temp: format!("{:.1}°", item.t.unwrap_or_default()),
            wind,
        })
    }
    // Daily forecast
    let mut daily = Vec::<WeatherForecast>::new();
    for item in response.properties.daily_forecast {
        let date = item.time; // TODO Formatting
                              // No wind for daily forecast
        let wind = String::new();
        daily.push(WeatherForecast {
            date,
            icon: icon_to_image(&item.daily_weather_icon.unwrap_or_default()),
            temp: format!(
                "{:.0}°/{:.0}°",
                item.t_min.unwrap_or_default(),
                item.t_max.unwrap_or_default()
            ),
            wind,
        })
    }
    Ok(Weather {
        report,
        hourly,
        daily,
    })
}

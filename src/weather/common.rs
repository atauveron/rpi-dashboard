use chrono::{offset::Local, DateTime, NaiveDateTime, TimeZone};

/// Convert a Unix timestamp to a localised DateTime
pub fn timestamp_to_datetime(ts: i64) -> Option<DateTime<Local>> {
    let utc = match NaiveDateTime::from_timestamp_opt(ts, 0) {
        Some(dt) => dt,
        None => return None,
    };
    Some(Local.from_utc_datetime(&utc))
}

/// Convert wind direction from degrees to text
/// Both are the direction the wind comes from.
pub fn wind_direction_to_text(wind_deg: u16) -> String {
    match wind_deg {
        0..=22 => String::from("N"),
        23..=67 => String::from("NE"),
        68..=112 => String::from("E"),
        113..=157 => String::from("SE"),
        158..=202 => String::from("S"),
        203..=247 => String::from("SW"),
        248..=292 => String::from("W"),
        293..=337 => String::from("NW"),
        338..=360 => String::from("N"),
        _ => String::from("?"),
    }
}

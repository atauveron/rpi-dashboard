mod common;
mod meteofrance;
mod openweather;

// Re-exports
pub use meteofrance::get_meteofrance;
pub use openweather::get_openweather;

/// A weather report
pub struct WeatherReport {
    /// The icon for the weather, as a `BinaryColor`-encoded image
    pub icon: &'static [u8; 288],
    /// The temperature
    pub temp: String,
    /// The wind
    pub wind: String,
}

// A weather forecast
#[derive(Clone)]
pub struct WeatherForecast {
    // The date of the forecast
    pub date: String,
    /// The icon for the weather, as a `BinaryColor`-encoded image
    pub icon: &'static [u8; 288],
    /// The temperature
    pub temp: String,
    /// The wind
    pub wind: String,
}

/// Weather information
pub struct Weather {
    pub report: WeatherReport,
    pub hourly: Vec<WeatherForecast>,
    pub daily: Vec<WeatherForecast>,
}

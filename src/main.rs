#![forbid(unsafe_code)]

#[macro_use]
extern crate lazy_static;

mod config;
mod display;
mod transport;
mod weather;

use std::fs::OpenOptions;

use chrono::{DateTime, Local};
use embedded_graphics::{
    image::{Image, ImageRaw},
    mono_font::iso_8859_1::{FONT_10X20, FONT_6X10, FONT_9X18_BOLD},
    mono_font::MonoTextStyleBuilder,
    pixelcolor::BinaryColor,
    prelude::*,
    text::Text,
};
use embedded_hal::prelude::*;
use epd_waveshare::{color::*, epd4in2::Display4in2, prelude::*};
use linux_embedded_hal::{
    spidev::{SpiModeFlags, SpidevOptions},
    {Delay, Spidev},
};
use log::LevelFilter;
use simplelog::{ConfigBuilder, WriteLogger};

// Half font width, for centered text
static HALF_FONT_WIDTH: f32 = 4.5;
// Text offset, for left-aligned text
static OFFSET: i32 = 2;

fn main() {
    // Read the configuration
    let config = config::read_config();

    // Initialise logging
    let _ = WriteLogger::init(
        LevelFilter::Info,
        ConfigBuilder::new().set_time_format_rfc3339().build(),
        OpenOptions::new()
            .append(true)
            .open("/var/log/dashboard.log")
            .unwrap(),
    );

    // Retrieve information
    let now = Local::now();
    let weather = weather::get_openweather(config.lat, config.lon, &config.openweather_key);
    let trains = transport::get_trains(&config.station_name, &config.station_id);

    // Prepare information:
    // * weather report
    // * next two hourly forecasts (skipping first element which is current hourly forecast),
    // and next daily forecast (skipping first element which is current daily forecast)
    // * upcoming trains
    let (report_opt, forecast_opt) = match weather {
        Ok(result) => {
            let mut forecast = Vec::<weather::WeatherForecast>::new();
            for item in result.hourly.iter().skip(1).take(2) {
                forecast.push(item.clone());
            }
            for item in result.daily.iter().skip(1).take(1) {
                forecast.push(item.clone());
            }
            (Some(result.report), Some(forecast))
        }
        Err(err) => {
            log::warn!("Error retrieving weather report: {}", err);
            (None, None)
        }
    };
    let trains_opt = match trains {
        Ok(result) => Some(result),
        Err(err) => {
            log::warn!("Error retrieving next trains: {}", err);
            None
        }
    };

    // Configure SPI
    let mut spi = Spidev::open("/dev/spidev0.0").expect("Failure - SPI directory");
    let options = SpidevOptions::new()
        .bits_per_word(8)
        .max_speed_hz(4_000_000)
        .mode(SpiModeFlags::SPI_MODE_0)
        .build();
    spi.configure(&options)
        .expect("Failure - SPI configuration");

    // Initialise display
    let mut delay = Delay {};
    let mut epd = display::init(&mut spi, &mut delay);

    // Update display, using retrieved information
    let display = draw_frame(&now, report_opt, forecast_opt, trains_opt);
    match epd.update_and_display_frame(&mut spi, display.buffer(), &mut delay) {
        Ok(_) => (),
        Err(err) => log::error!("Failure - Display: {}", err.to_string()),
    };
    delay.delay_ms(50u16);

    // Shut down display
    epd.sleep(&mut spi, &mut delay)
        .expect("Failure - E-Ink sleep");
}

fn draw_frame(
    date: &DateTime<Local>,
    weather_report: Option<weather::WeatherReport>,
    weather_forecast: Option<Vec<weather::WeatherForecast>>,
    trains: Option<Vec<transport::Train>>,
) -> Display4in2 {
    let mut display = Display4in2::default();
    let transport_style = MonoTextStyleBuilder::new()
        .font(&FONT_9X18_BOLD)
        .text_color(Black)
        .background_color(White)
        .build();
    let weather_style = MonoTextStyleBuilder::new()
        .font(&FONT_10X20)
        .text_color(Black)
        .background_color(White)
        .build();
    let date_style = MonoTextStyleBuilder::new()
        .font(&FONT_6X10)
        .text_color(Black)
        .background_color(White)
        .build();

    // Display the current weather (centered)
    // Center the icon
    match weather_report {
        None => {
            let _ = Text::new("Weather", Point::new(OFFSET, 30), weather_style).draw(&mut display);
            let _ = Text::new("report", Point::new(OFFSET, 50), weather_style).draw(&mut display);
            let _ = Text::new("not", Point::new(OFFSET, 70), weather_style).draw(&mut display);
            let _ =
                Text::new("available", Point::new(OFFSET, 90), weather_style).draw(&mut display);
        }
        Some(weather) => {
            let image_raw: ImageRaw<BinaryColor> = ImageRaw::new(weather.icon, 48);
            let image = Image::new(&image_raw, Point::new(26, 30));
            let _ = image.draw(&mut display);
            let _ =
                Text::new(&weather.temp, Point::new(OFFSET, 100), weather_style).draw(&mut display);
            let _ =
                Text::new(&weather.wind, Point::new(OFFSET, 120), weather_style).draw(&mut display);
        }
    }

    // Display the weather forecasts (maximum 3)
    // Center the dates and icons
    match weather_forecast {
        None => {
            let _ = Text::new(
                "Weather forecast not available",
                Point::new(105, 30),
                weather_style,
            )
            .draw(&mut display);
        }
        Some(forecasts) => {
            for (i, forecast) in forecasts.iter().enumerate().take(3) {
                let _ = Text::new(
                    &forecast.date,
                    Point::new(
                        100 * (i as i32 + 1) + 50
                            - (HALF_FONT_WIDTH * forecast.date.len() as f32) as i32,
                        20,
                    ),
                    weather_style,
                )
                .draw(&mut display);
                let image_raw: ImageRaw<BinaryColor> = ImageRaw::new(forecast.icon, 48);
                let image = Image::new(&image_raw, Point::new(100 * (i as i32 + 1) + 26, 30));
                let _ = image.draw(&mut display);
                let _ = Text::new(
                    &forecast.temp,
                    Point::new(100 * (i as i32 + 1) + OFFSET, 100),
                    weather_style,
                )
                .draw(&mut display);
                let _ = Text::new(
                    &forecast.wind,
                    Point::new(100 * (i as i32 + 1) + OFFSET, 120),
                    weather_style,
                )
                .draw(&mut display);
            }
        }
    }

    // Display the next trains (maximum 7)
    match trains {
        None => {
            let _ = Text::new("No trains found", Point::new(5, 150), transport_style)
                .draw(&mut display);
        }
        Some(trains) => {
            for (i, train) in trains.iter().enumerate().take(7) {
                let _ = Text::new(
                    &format!(
                        "{}{} {} {}",
                        if train.has_disruption { '!' } else { ' ' },
                        train.time,
                        train.mission,
                        train.destination
                    ),
                    Point::new(0, 150 + 20 * (i as i32)),
                    transport_style,
                )
                .draw(&mut display);
            }
        }
    }

    // Display the refresh date (centered)
    let _ = Text::new(
        &format!("{}", date.format("%Y-%m-%d %H:%M")),
        Point::new(152, 295),
        date_style,
    )
    .draw(&mut display);

    // Return
    display
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn meteofrance() {
        // The config file must contain a valid configuration.
        let config = config::read_config();
        weather::get_meteofrance(config.lat, config.lon, None).unwrap();
    }

    #[test]
    fn openweather() {
        // The config file must contain a valid configuration.
        let config = config::read_config();
        weather::get_openweather(config.lat, config.lon, &config.openweather_key).unwrap();
    }

    #[test]
    fn transport() {
        // The config file must contain a valid configuration.
        let config = config::read_config();
        transport::get_trains(&config.station_name, &config.station_id).unwrap();
    }
}

# rpi-dashboard

An e-ink dashboard using a Raspberry Pi

This dashboard displays weather information (current weather and forecast) as well as public-transport information (next Transilien trains from a given station).

## Hardware setup

The e-ink display ([Waveshare's 4.2inch e-Paper Module](https://www.waveshare.com/wiki/4.2inch_e-Paper_Module) connected using [Waveshare's E-Paper Driver HAT](https://www.waveshare.com/wiki/E-Paper_Driver_HAT)) must be connected to the Raspberry Pi following the manufacturer's instructions. I use :

* SPI device `/dev/spidev0.0`,
* pin 5 (Broadcom) for CS,
* pin 24 (Broadcom) for BUSY,
* pin 15 (Broadcom) for DC,
* pin 17 (Broadcom) for RST.

## Software setup

Copy the sample configuration file `config.json.sample` to `config.json` and fill in the required information.

The weather information is retrieved from [OpenWeather](https://openweathermap.org/), using their [One Call API](https://openweathermap.org/api/one-call-api).

The public-transport information is retrieved from the [Transilien website](https://www.transilien.com/).

## Icons

Original icons made by [bqlqn](https://www.flaticon.com/authors/bqlqn) from [www.flaticon.com](https://www.flaticon.com/).

The `BinaryColor`-encoded versions (in `assets`) were generated using my `image-to-binary-color` CLI utility.

## Compiling

To cross-compile for a Raspberry Pi, use the following command (after installing the toolchain):

```sh
cargo build --release --target aarch64-unknown-linux-gnu
```

Or using [cross](https://github.com/cross-rs/cross):

```sh
cross build --release --target aarch64-unknown-linux-gnu
```
